export let taskController = {
  renderTask: (taskList) => {
    let contentHTML = "";
    for (let index = 0; index < taskList.length; index++) {
      let task = taskList[index];
      let contentTR = `<li>
                        <div>${task.name}</div>
                        <div class="buttons">
                            <button onclick="removeTask(${task.id})" class="remove"><i class="fa fa-trash-alt"></i></button>
                            <button onclick="completeTask(${task.id})" class="complete"><i class="fa fa-check-circle"></i></button>
                        </div>
                    </li>`;
      contentHTML = contentHTML + contentTR;
    }
    document.getElementById("todo").innerHTML = contentHTML;
  },

  renderCompleteTask: (taskList) => {
    let contentHTML = "";
    for (let index = 0; index < taskList.length; index++) {
      let task = taskList[index];
      let contentTR = `<li>
                        <div>${task.name}</div>
                        <div class="buttons">
                            <button onclick="removeCompletedTask(${task.id})" class="remove"><i class="fa fa-trash-alt"></i></button>
                            <button class="complete"><i class="fa fa-check-circle"></i></button>
                        </div>
                    </li>`;
      contentHTML = contentHTML + contentTR;
    }

    document.getElementById("completed").innerHTML = contentHTML;
  },

  getInputTask: () => {
    let taskName = document.getElementById("newTask").value;
    let task = {
      name: taskName,
    };
    return task;
  },
};
