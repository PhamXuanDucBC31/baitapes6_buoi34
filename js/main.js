import { taskController } from "../controller/taskController.js";
import { taskService } from "../service/taskService.js";

let taskList = [];
let completedTaskList = [];

let renderTaskToHTML = () => {
  taskService
    .getTaskList()
    .then((res) => {
      taskList = res.data;
      taskController.renderTask(taskList);
    })
    .catch((err) => {});
};
renderTaskToHTML();

let removeTask = (taskID) => {
  taskService
    .removeTask(taskID)
    .then((res) => {
      renderTaskToHTML();
    })
    .catch((err) => {});
};
window.removeTask = removeTask;

let addTask = () => {
  let newTask = taskController.getInputTask();
  taskService
    .addNewTask(newTask)
    .then((res) => {
      renderTaskToHTML();
    })
    .catch((err) => {});
};
window.addTask = addTask;

let renderCompleteTask = () => {
  taskService
    .getNewCompletedTask()
    .then((res) => {
      completedTaskList = res.data;
      taskController.renderCompleteTask(completedTaskList);
    })
    .catch((err) => {});
};
renderCompleteTask();

let completeTask = (taskId) => {
  for (let index = 0; index < taskList.length; index++) {
    if (taskList[index].id == taskId) {
      taskService
        .addNewCompletedTask(taskList[index])
        .then((res) => {
          removeTask(taskId);
          renderCompleteTask();
        })
        .catch((err) => {});
    }
  }
};
window.completeTask = completeTask;

let removeCompletedTask = (taskId) => {
  taskService
    .deleteCompletedTask(taskId)
    .then((res) => {
      renderCompleteTask();
    })
    .catch((err) => {});
};
window.removeCompletedTask = removeCompletedTask;
