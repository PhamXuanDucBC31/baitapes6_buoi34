const base_url = "https://62b07878196a9e9870244798.mockapi.io/to-do-list";
const completed_task_url =
  "https://62b07878196a9e9870244798.mockapi.io/completed-task";

export let taskService = {
  getTaskList: () => {
    return axios({
      url: base_url,
      method: "GET",
    });
  },

  removeTask: (id) => {
    return axios({
      url: `${base_url}/${id}`,
      method: "DELETE",
    });
  },

  addNewTask: (task) => {
    return axios({
      url: base_url,
      method: "POST",
      data: task,
    });
  },

  getTaskInfo: (id) => {
    return axios({
      url: `${base_url}/${id}`,
      method: "GET",
    });
  },

  addNewCompletedTask: (task) => {
    return axios({
      url: completed_task_url,
      method: "POST",
      data: task,
    });
  },

  getNewCompletedTask: () => {
    return axios({
      url: completed_task_url,
      method: "GET",
    });
  },

  deleteCompletedTask: (id) => {
    return axios({
      url: `${completed_task_url}/${id}`,
      method: "DELETE",
    });
  },
};
